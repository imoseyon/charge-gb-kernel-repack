#!/bin/sh
echo "imoseyon_kernel_charge_gb_$1" > initramfs_root/kernel_version
rm -f /tmp/*cpio*
/data/utils/kernel_repack_utils/repacker.sh -s zImage -d /tmp/new_zImage -r initramfs_root -c gzip
cd /data/charge-froyo/zip
cp /tmp/new_zImage kernel_update/zImage
rm *.zip
zip -r imoseyon_kernel_charge_gb_$1.zip *
chown -R imoseyon *
cp *.zip /tmp
